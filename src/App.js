import React from 'react';
import PlayWidget from 'react-spotify-widgets';
import logo from './logo.svg';
import './App.css';


function getFutureWeather() {
    fetch("https://api.openweathermap.org/data/2.5/forecast?lat=53.366&lon=-1.816&appid=ac39edac43054d6a5db06a08f8e75c3e")
        .then(res => res.json())
        .then(
            (result) => {
                let daylightBlocks = result.list.filter(timeBlock => dayjs(timeBlock.dt_txt).hour() >= 9 && dayjs(timeBlock.dt_txt).hour() <= 15);
                let sunniestBlock = findClouds(daylightBlocks); //result.list.slice(result.list.length - 9, result.list.length - 1)
                let cloudCover = sunniestBlock.clouds.all.toString();
                console.log(result);
                console.log(daylightBlocks);
                console.log(sunniestBlock);

                //console.log(currentBlock);
                const backendApiUrl = "https://localhost:5001/api/WeatherBlocks";
                const data = {
                    "cloudCoverPercent": sunniestBlock.clouds.all,
                    "dateTimeTxt": sunniestBlock.dt_txt,
                    "tempActualKelvin": sunniestBlock.main.temp
                }
                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json'},
                    body: JSON.stringify(data)
                };
                fetch(backendApiUrl, requestOptions)
                    .then(response => response.json())
                    //.then(data => this.setState({ postId: data.id }));
                    .then(
                        (result) => { console.log(result) }
                    );
                alert(cloudCover);
            });
    return alert('Book that vaycay!');
}

function getCurrentWeather() {
    fetch("https://api.openweathermap.org/data/2.5/weather?lat=53.366&lon=-1.816&appid=ac39edac43054d6a5db06a08f8e75c3e")
        .then(res => res.json())
        .then(
            (result) => {
                let cloudCover = result.clouds.all.toString();
                console.log(result);
                const backendApiUrl = "https://localhost:5001/api/WeatherBlocks";
                const data = {
                    "cloudCoverPercent": result.clouds.all,
                    "dateTimeTxt": result.dt_txt,
                    "tempActualKelvin": result.main.temp
                }
                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                };
                fetch(backendApiUrl, requestOptions)
                    .then(response => response.json())
                    //.then(data => this.setState({ postId: data.id }));
                    .then(
                        (result) => { console.log(result) }
                    );
                alert(cloudCover);
            });
    return alert('What\'s that weather?');
}

function findClouds(openWeatherInput) {
    
    return openWeatherInput.reduce((accumulator, current) => {
        return accumulator.clouds.all > current.clouds.all ? current : accumulator;
    }, openWeatherInput[openWeatherInput.length -1]);
   
}

var dayjs = require('dayjs');
const TodaysDate = function () {
    return dayjs().format('dddd MMMM DD HH:mm');
};

const FiveDaysTime = function () {
    return dayjs().add(5, 'day').format('dddd MMMM DD HH:mm');
};

const CurrentDate = (props) => {
    return (
        <div>
            <p>The current date is:</p>
            <TodaysDate /><br />
            <p>The date in 5 days will be: </p> 
            <FiveDaysTime />
        </div>
    );
};
   
const Logo = function () {
    return <img src={logo} className="App-logo" alt="logo" />
};

const CurrentWeatherButton = function() {
    return (
        <p id="current-weather-button"> 
            OMG <br />
            <button onClick= {getCurrentWeather}>What's The Weather Now?</button> <br />
            BBQ!
        </p >
    );

}

const FutureWeatherButton = function () {
    return (
    <p id="future-weather-button">
        OMG <br />
        <button onClick={getFutureWeather}>What's The Weather in 5 Days?</button> <br />
    BBQ!
        </p >
);

}

class MusicPlayer extends React.Component {
    render() {
        return (
            <div className='App'>
                <PlayWidget
                    width={300}
                    height={80}
                    uri={'spotify:track:6dGnYIeXmHdcikdzNNDMm2'} />
            </div>
        );
    }
}

class Calendar extends React.Component {
  
    render() {
        return (
            <div>
                <h3>What date is it?</h3>
                <CurrentDate />
            </div>
        );
    }
};


const Link = function () {
    return (
        <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
        >
            Weather API!
        </a>
    );
}


function App() {
  return (
    <div className="App, App-header">
          <Logo />
          <Calendar />
          <CurrentWeatherButton />
          <FutureWeatherButton />
          <MusicPlayer />
        <Link />
    </div>
    );
}

export default App;




//class MyComponent extends React.Component {
//    constructor(props) {
//        super(props);
//        this.state = {
//            error: null,
//            isLoaded: false,
//            items: null
//        };
//    }

//    componentDidMount() {


//function getMusic() {
//    fetch('https://accounts.spotify.com/api/token', {
//        method: 'POST',
//        headers: {
//            'Authorization': 'Basic ZDQyMzQ1MTViNzRmNGYyNmFlZWI1YjBiNTAyNTRlNmY6OTdjNWFhNzA1NmRhNDdmNGI2OTAyMDEzMTk5ZTlkMjA=',
//            'Content-Type': 'application/json',
//        },
//        body: 'grant_type=client_credentials'
//    }).then(res => { console.log(res); res.json()})
//        .then(
//            (result) => {
//                console.log(result)
//            });

    //fetch("https://api.spotify.com/v1/tracks/6dGnYIeXmHdcikdzNNDMm2")
    //    .then(res => res.json())
    //    .then(
    //        (result) => {
    //            console.log(result)
    //        });
    //      }

//const MusicButton = function () {
//    return (
//        <p>
//            <button onClick={getMusic}>Play My Tune</button>
//        </p>
//        )
//}
