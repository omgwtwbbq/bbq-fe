﻿const functions = require("./../../src/helpers");

test('Adds 2 + 3 to equal 5', () => {
    expect(functions.add(2, 3)).toBe(5);
});